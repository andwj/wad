// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Reading and parsing of WAD files.

*/

package wad

import (
	"fmt"
	"io"
	"os"
	"strings"
)

// Open returns a new Wad struct, opening the given file, checking
// it is a valid WAD file, and reading its lump directory.
// None of the lumps are read into memory at this stage.
// Levels in the wad are detected but not decoded.
func Open(filename string) (*Wad, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	w := newWad(f, false /*isWrite*/)

	err = w.readDirectory()
	if err != nil {
		w.Close()
		return nil, err
	}

	// detect where the levels are
	for i := 0; i < len(w.Directory); i++ {
		w.detectLevelAt(i)
	}

	return w, nil
}

// Close closes the file which is associated with this wad,
// previously created by the Open() or Create() functions.
// The Wad struct will be invalid after this, and no further
// methods should be called on it.
func (w *Wad) Close() {
	if w.file != nil {
		w.file.Close()
		w.file = nil

		w.Directory = nil
	}
}

// FindLump finds a lump with the given name.  It returns a Lump
// pointer when found, or nil otherwise.  The given name should
// be uppercase.  If there are multiple lumps have the same name,
// the first one in the wad directory is returned.
func (w *Wad) FindLump(name string) *Lump {
	for _, lump := range w.Directory {
		if lump.Name == name {
			return lump
		}
	}
	return nil
}

// ReadLump reads the contents of the given lump into memory,
// returning a byte slice on success, or an error on failure.
func (w *Wad) ReadLump(lump *Lump) ([]byte, error) {
	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return nil, err
	}

	buf := make([]byte, int(lump.Length))

	err = readExact(w.file, buf)
	if err != nil {
		return nil, err
	}

	return buf, nil
}

// ReadLumpPartial reads up to 'size' bytes from the start the given
// lump into memory, returning a byte slice on success, or an error on
// failure.  Less data is returned only when the lump is smaller.
func (w *Wad) ReadLumpPartial(lump *Lump, size int) ([]byte, error) {
	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return nil, err
	}

	if int(lump.Length) < size {
		size = int(lump.Length)
	}

	buf := make([]byte, size)

	err = readExact(w.file, buf)
	if err != nil {
		return nil, err
	}

	return buf, nil
}

// ReadLevel decodes the level which begins at the given
// index in the wad directory.  Returns a pointer to the
// decoded Level on success, or nil and an error on failure.
// Be aware that UDMF format levels are NOT supported.
func (w *Wad) ReadLevel(lumpIndex int) (*Level, error) {
	format := w.Directory[lumpIndex].LevelMarker
	if format == NOT_A_LEVEL {
		return nil, fmt.Errorf("bad lumpIndex (not a level)")
	}

	if format == FORMAT_UDMF {
		return nil, fmt.Errorf("UDMF levels are not supported")
	}

	lev := NewLevel(w.Directory[lumpIndex].Name, format)

	var err error
	lev.HeaderData, err = w.ReadLump(w.Directory[lumpIndex])
	if err != nil {
		return nil, err
	}

	if err := w.readThings(lev, w.Directory[lumpIndex+LEVOF_Things]); err != nil {
		return nil, err
	}

	if err := w.readVertexes(lev, w.Directory[lumpIndex+LEVOF_Vertexes]); err != nil {
		return nil, err
	}

	if err := w.readSectors(lev, w.Directory[lumpIndex+LEVOF_Sectors]); err != nil {
		return nil, err
	}

	if err := w.readSideDefs(lev, w.Directory[lumpIndex+LEVOF_SideDefs]); err != nil {
		return nil, err
	}

	if err := w.readLineDefs(lev, w.Directory[lumpIndex+LEVOF_LineDefs]); err != nil {
		return nil, err
	}

	if format == FORMAT_Hexen {
		var err error

		lev.Behavior, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_Behavior])
		if err != nil {
			return nil, err
		}

		// the SCRIPTS lump is optional, check if present
		scrIdx := lumpIndex + LEVOF_Scripts

		if scrIdx < len(w.Directory) && w.Directory[scrIdx].Name == "SCRIPTS" {
			lev.Scripts, err = w.ReadLump(w.Directory[scrIdx])
			if err != nil {
				return nil, err
			}
		}
	}

	if err := w.readNodes(lev, lumpIndex); err != nil {
		return nil, err
	}

	return lev, nil // OK
}

func (w *Wad) readDirectory() error {
	// read the header, verify it is really a WAD file
	var ident [4]byte
	var start int32
	var count int32

	err := w.readBunch(ident[:], &count, &start)
	if err != nil {
		return err
	}

	if ident[0] == 'I' {
		w.Iwad = true
	}

	if !(ident[1] == 'W' && ident[2] == 'A' && ident[3] == 'D') {
		return fmt.Errorf("invalid wad file (bad ident in header)")
	}
	if start < 12 {
		return fmt.Errorf("invalid wad file (bad directory start)")
	}
	if count < 0 || count > 32000 {
		return fmt.Errorf("invalid wad file (bad directory count)")
	}

	// seek to beginning of directory and read each entry
	_, err = w.file.Seek(int64(start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := int32(0); i < count; i++ {
		lump := w.newLump()

		err = w.readBunch(&lump.Start, &lump.Length, &lump.Name)
		if err != nil {
			return err
		}
	}

	return nil // OK
}

func (w *Wad) detectLevelAt(pos int) {
	if w.detectUDMFLevelAt(pos) {
		return
	}

	// skip the marker lump
	end := pos + 1

	match := true &&
		w.matchNextLump(&end, "THINGS") &&
		w.matchNextLump(&end, "LINEDEFS") &&
		w.matchNextLump(&end, "SIDEDEFS") &&
		w.matchNextLump(&end, "VERTEXES") &&
		w.matchNextLump(&end, "SEGS") &&
		w.matchNextLump(&end, "SSECTORS") &&
		w.matchNextLump(&end, "NODES") &&
		w.matchNextLump(&end, "SECTORS") &&
		w.matchNextLump(&end, "REJECT") &&
		w.matchNextLump(&end, "BLOCKMAP")

	if match {
		w.markLevelAt(pos, end)
	}
}

func (w *Wad) markLevelAt(pos, end int) {
	w.Directory[pos].LevelMarker = FORMAT_Doom

	if w.matchNextLump(&end, "BEHAVIOR") {
		w.Directory[pos].LevelMarker = FORMAT_Hexen

		// this is usually not present, but it should be treated
		// as part of the level when it is.
		w.matchNextLump(&end, "SCRIPTS")
	}

	// mark GL-Nodes following the level as part of it
	if end < len(w.Directory) && strings.HasPrefix(w.Directory[end].Name, "GL_") {
		old_end := end

		end++

		match_gl := true &&
			w.matchNextLump(&end, "GL_VERT") &&
			w.matchNextLump(&end, "GL_SEGS") &&
			w.matchNextLump(&end, "GL_SSECT") &&
			w.matchNextLump(&end, "GL_NODES")

		if match_gl {
			// the GL_PVS lump is optional with GL-Nodes
			w.matchNextLump(&end, "GL_PVS")

			w.Directory[old_end].GLMarker = true
		} else {
			// reset end position
			end = old_end
		}
	}

	for i := pos + 1; i < end; i++ {
		w.Directory[i].LevelPart = true
	}
}

func (w *Wad) detectUDMFLevelAt(pos int) bool {
	// skip the marker lump
	end := pos + 1

	if !w.matchNextLump(&end, "TEXTMAP") {
		return false
	}

	// limit how far we look ahead for the ENDMAP
	end_limit := pos + 40

	for !w.matchNextLump(&end, "ENDMAP") {
		// no more lumps? (map is unclosed)
		if end >= len(w.Directory) || end > end_limit {
			return false
		}

		// hit another UDMF map? (map is unclosed)
		if w.matchNextLump(&end, "TEXTMAP") {
			return false
		}

		end += 1
	}

	// ok, we have a UDMF level

	w.Directory[pos].LevelMarker = FORMAT_UDMF

	for i := pos + 1; i < end; i++ {
		w.Directory[i].LevelPart = true

		if strings.HasPrefix(w.Directory[i].Name, "GL_") {
			w.Directory[i].GLMarker = true
		}
	}

	return true
}

func (w *Wad) matchNextLump(cur *int, name string) bool {
	if *cur >= len(w.Directory) {
		return false
	}
	if w.Directory[*cur].Name != name {
		return false
	}
	*cur += 1
	return true
}

func (w *Wad) readVertexes(lev *Level, lump *Lump) error {
	count := int(lump.Length) / 4
	if cap(lev.Verts) < count {
		lev.Verts = make([]*Vertex, 0, count)
	}

	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		v := lev.NewVertex()
		v.Index = i

		err = w.readBunch(&v.X, &v.Y)
		if err != nil {
			return err
		}
	}

	return nil // OK
}

func (w *Wad) readThings(lev *Level, lump *Lump) error {
	size := 10
	if lev.Format == FORMAT_Hexen {
		size = 20
	}

	count := int(lump.Length) / size
	if cap(lev.Things) < count {
		lev.Things = make([]*Thing, 0, count)
	}

	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		t := lev.NewThing()
		t.Index = i

		switch lev.Format {
		case FORMAT_Doom:
			err = w.readBunch(&t.X, &t.Y, &t.Angle, &t.Kind, &t.Flags)

		case FORMAT_Hexen:
			err = w.readBunch(&t.Tid, &t.X, &t.Y, &t.Height,
				&t.Angle, &t.Kind, &t.Flags,
				&t.Special, t.Args[:])
		}

		if err != nil {
			return err
		}
	}

	return nil // OK
}

func (w *Wad) readSectors(lev *Level, lump *Lump) error {
	count := int(lump.Length) / 26
	if cap(lev.Sectors) < count {
		lev.Sectors = make([]*Sector, 0, count)
	}

	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		s := lev.NewSector()
		s.Index = i

		err = w.readBunch(&s.FloorH, &s.CeilH,
			&s.FloorTex, &s.CeilTex,
			&s.Light, &s.Special, &s.Tag)

		if err != nil {
			return err
		}
	}

	return nil // OK
}

func (w *Wad) readLineDefs(lev *Level, lump *Lump) error {
	size := 14
	if lev.Format == FORMAT_Hexen {
		size = 16
	}

	count := int(lump.Length) / size
	if cap(lev.Lines) < count {
		lev.Lines = make([]*LineDef, 0, count)
	}

	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		ld := lev.NewLineDef()
		ld.Index = i

		var start, end uint16
		var front, back uint16

		switch lev.Format {
		case FORMAT_Doom:
			err = w.readBunch(&start, &end, &ld.Flags, &ld.Special,
				&ld.Tag, &front, &back)

		case FORMAT_Hexen:
			var spec8 uint8
			err = w.readBunch(&start, &end, &ld.Flags, &spec8,
				ld.Args[:], &front, &back)
			ld.Special = uint16(spec8)
		}

		if err != nil {
			return err
		}

		// lookup vertices
		if int(start) >= len(lev.Verts) || int(end) >= len(lev.Verts) {
			return fmt.Errorf("bad vertex ref in linedef #%d", i)
		}

		ld.Start = lev.Verts[start]
		ld.End = lev.Verts[end]

		// lookup sidedefs
		if front != _NO_SIDEDEF {
			if int(front) >= len(lev.Sides) {
				return fmt.Errorf("bad sidedef ref in linedef #%d", i)
			}
			ld.Front = lev.Sides[front]
		}

		if back != _NO_SIDEDEF {
			if int(back) >= len(lev.Sides) {
				return fmt.Errorf("bad sidedef ref in linedef #%d", i)
			}
			ld.Back = lev.Sides[back]
		}
	}

	return nil // OK
}

func (w *Wad) readSideDefs(lev *Level, lump *Lump) error {
	count := int(lump.Length) / 30
	if cap(lev.Sides) < count {
		lev.Sides = make([]*SideDef, 0, count)
	}

	_, err := w.file.Seek(int64(lump.Start), io.SeekStart)
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		sd := lev.NewSideDef()
		sd.Index = i

		var sector uint16

		err = w.readBunch(&sd.OffsetX, &sd.OffsetY,
			&sd.Upper, &sd.Lower, &sd.Mid,
			&sector)

		if err != nil {
			return err
		}

		// lookup the sector
		if int(sector) >= len(lev.Sectors) {
			return fmt.Errorf("bad sector ref in sidedef #%d", i)
		}

		sd.Sector = lev.Sectors[sector]
	}

	return nil // OK
}

func (w *Wad) readNodes(lev *Level, lumpIndex int) error {
	var err error

	nodes := NodeInfo{}

	nodes.segs, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_Segs])
	if err != nil {
		return err
	}

	nodes.ssectors, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_SSectors])
	if err != nil {
		return err
	}

	nodes.nodes, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_Nodes])
	if err != nil {
		return err
	}

	nodes.reject, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_Reject])
	if err != nil {
		return err
	}

	nodes.blockmap, err = w.ReadLump(w.Directory[lumpIndex+LEVOF_Blockmap])
	if err != nil {
		return err
	}

	lev.Nodes = &nodes

	// check if GL-Nodes exist, if so then load them
	start := lumpIndex + LEVOF_Blockmap

	for ; start < len(w.Directory) && w.Directory[start].LevelPart; start++ {
		if w.Directory[start].GLMarker {
			nodes.HasGL = true

			nodes.gl_marker_name = w.Directory[start].Name

			if err := w.readGLNodes(lev, start, &nodes); err != nil {
				return err
			}
		}
	}

	return nil // OK
}

func (w *Wad) readGLNodes(lev *Level, start int, nodes *NodeInfo) error {
	var err error

	nodes.gl_marker_data, err = w.ReadLump(w.Directory[start])
	if err != nil {
		return err
	}

	nodes.gl_vert, err = w.ReadLump(w.Directory[start+glofs_Vert])
	if err != nil {
		return err
	}

	nodes.gl_segs, err = w.ReadLump(w.Directory[start+glofs_Segs])
	if err != nil {
		return err
	}

	nodes.gl_ssect, err = w.ReadLump(w.Directory[start+glofs_SSect])
	if err != nil {
		return err
	}

	nodes.gl_nodes, err = w.ReadLump(w.Directory[start+glofs_Nodes])
	if err != nil {
		return err
	}

	// the GL_PVS lump is optional, check it exists first
	if start+glofs_PVS < len(w.Directory) &&
		w.Directory[start+glofs_PVS].Name == "GL_PVS" {

		nodes.gl_pvs, err = w.ReadLump(w.Directory[start+glofs_PVS])
		if err != nil {
			return err
		}
	}

	return nil
}

/* UTILITIES */

// readBunch is a variadic function which will handle 8/16/32-bit
// values and 8-byte strings and read them appropriately.  If any
// read produces an error, that error will be immediately returned.
func (w *Wad) readBunch(a ...interface{}) error {
	for _, arg := range a {
		var err error

		switch ptr := arg.(type) {
		case *uint8:
			*ptr, err = w.read8()
		case *uint16:
			*ptr, err = w.read16()
		case *uint32:
			*ptr, err = w.read32()

		case *int16:
			var temp uint16
			temp, err = w.read16()
			*ptr = int16(temp)
		case *int32:
			var temp uint32
			temp, err = w.read32()
			*ptr = int32(temp)

		case *ThingFlags:
			var temp uint16
			temp, err = w.read16()
			*ptr = ThingFlags(temp)
		case *LineFlags:
			var temp uint16
			temp, err = w.read16()
			*ptr = LineFlags(temp)

		case *string:
			*ptr, err = w.readStr8()
		case []byte:
			err = readExact(w.file, ptr)

		default:
			fmt.Printf("\nType not supported: %t\n", ptr)
			panic("readBunch: unsupported type used")
		}
		if err != nil {
			return err
		}
	}
	return nil // OK
}

// read an 8-bit byte
func (w *Wad) read8() (uint8, error) {
	var buf [1]byte
	err := readExact(w.file, buf[:])
	return buf[0], err
}

// read an 16-bit little endian byte
func (w *Wad) read16() (uint16, error) {
	var buf [2]byte

	err := readExact(w.file, buf[:])
	if err != nil {
		return 0, err
	}

	val := uint16(buf[0]) | (uint16(buf[1]) << 8)

	return val, nil // OK
}

// read an 32-bit little endian byte
func (w *Wad) read32() (uint32, error) {
	var buf [4]byte

	err := readExact(w.file, buf[:])
	if err != nil {
		return 0, err
	}

	val := uint32(buf[0]) |
		(uint32(buf[1]) << 8) |
		(uint32(buf[2]) << 16) |
		(uint32(buf[3]) << 24)

	return val, nil // OK
}

// read a short string stored in 8 bytes, padded with zeros
func (w *Wad) readStr8() (string, error) {
	var buf [8]byte

	err := readExact(w.file, buf[:])
	if err != nil {
		return "", err
	}

	// determine length
	leng := 0
	for ; leng < 8; leng++ {
		if buf[leng] == 0 {
			break
		}
	}

	// convert to uppercase
	s := string(buf[0:leng])

	return strings.ToUpper(s), nil
}

// read the exact size of the buffer, or return an error
// [ unfortunately this is missing from the standard library ]
func readExact(f *os.File, b []byte) error {
	if len(b) == 0 {
		return nil
	}

	for {
		count, err := f.Read(b)

		// ugh, seems Read may return some data AND the EOF error
		if count > 0 {
			b = b[count:]

			if len(b) == 0 && (err == nil || err == io.EOF) {
				return nil
			}
		}
		if err != nil {
			return err
		}
	}
}
