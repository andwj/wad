// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Creating and writing support for WAD files.

*/

package wad

import (
	"fmt"
	"io"
	"os"
	"strings"
)

// Create opens the given file for writing, and returns a Wad
// struct that can be used for writing lumps and levels into the
// freshly created WAD file.
func Create(filename string) (*Wad, error) {
	f, err := os.Create(filename)
	if err != nil {
		return nil, err
	}

	w := newWad(f, true /*isWrite*/)

	// write a dummy header (the real header is written by writeDirectory)
	err = w.writeBunch(uint32(0), uint32(0), uint32(0))
	if err != nil {
		w.Close()
		return nil, err
	}

	return w, nil
}

// Finish causes the wad directory to be written into the file.
// This must only be called once, and no more lumps or levels
// can be written to the wad after this.  You still need to call
// Close() on the wad.
func (w *Wad) Finish() error {
	if w.file == nil {
		panic("called Finish on a closed wad")
	}
	if !w.isWrite {
		panic("called Finish on a reader wad")
	}

	return w.writeDirectory()
}

func (w *Wad) writeDirectory() error {
	err := w.writePad4()
	if err != nil {
		return err
	}

	// get the file offset of the directory
	offset, err := w.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	for _, lump := range w.Directory {
		err = w.writeBunch(lump.Start, lump.Length, lump.Name)

		if err != nil {
			return err
		}
	}

	// seek back to beginning of file to write the real WAD header,
	// which contains the directory offset and number of lumps.

	_, err = w.file.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	header := [4]byte{'P', 'W', 'A', 'D'}

	if w.Iwad {
		header[0] = 'I'
	}

	lumps := len(w.Directory)

	return w.writeBunch(header[:], uint32(lumps), uint32(offset))
}

// WriteLump saves a lump of raw bytes into the wad's file, and
// adds an entry into the wad's internal directory.
// The 'data' slice can be nil to create an empty lump.
func (w *Wad) WriteLump(name string, data []byte) error {
	err := w.beginLump(name)
	if err != nil {
		return err
	}

	if data != nil {
		err = writeExact(w.file, data)
		if err != nil {
			return err
		}
	}

	return w.endLump()
}

// WriteLevel encodes the given Level data into the wad, creating
// numerous lumps which are added to the wad's internal directory.
// If any lumps fail to be written, an error is returned.
// Be aware that UDMF format levels are NOT supported.
func (w *Wad) WriteLevel(lev *Level) error {
	var err error

	if !w.isWrite {
		panic("called WriteLevel on a reader Wad")
	}

	if lev.Format == FORMAT_UDMF {
		return fmt.Errorf("UDMF levels are not supported")
	}

	// set the correct Index values for references
	lev.setFinalIndices()

	// create the marker lump (it is usually empty)
	err = w.WriteLump(lev.Name, lev.HeaderData)
	if err != nil {
		return err
	}

	err = w.writeThings(lev)
	if err != nil {
		return err
	}

	err = w.writeLineDefs(lev)
	if err != nil {
		return err
	}

	err = w.writeSideDefs(lev)
	if err != nil {
		return err
	}

	err = w.writeVertexes(lev)
	if err != nil {
		return err
	}

	if lev.Nodes != nil {
		err = w.WriteLump("SEGS", lev.Nodes.segs)
	} else {
		err = w.WriteLump("SEGS", nil)
	}
	if err != nil {
		return err
	}

	if lev.Nodes != nil {
		err = w.WriteLump("SSECTORS", lev.Nodes.ssectors)
	} else {
		err = w.WriteLump("SSECTORS", nil)
	}
	if err != nil {
		return err
	}

	if lev.Nodes != nil {
		err = w.WriteLump("NODES", lev.Nodes.nodes)
	} else {
		err = w.WriteLump("NODES", nil)
	}
	if err != nil {
		return err
	}

	err = w.writeSectors(lev)
	if err != nil {
		return err
	}

	if lev.Nodes != nil {
		err = w.WriteLump("REJECT", lev.Nodes.reject)
	} else {
		err = w.WriteLump("REJECT", nil)
	}
	if err != nil {
		return err
	}

	if lev.Nodes != nil {
		err = w.WriteLump("BLOCKMAP", lev.Nodes.blockmap)
	} else {
		err = w.WriteLump("BLOCKMAP", nil)
	}
	if err != nil {
		return err
	}

	if lev.Format == FORMAT_Hexen {
		if len(lev.Behavior) > 0 {
			err = w.WriteLump("BEHAVIOR", lev.Behavior)
		} else {
			err = w.writeMinimalBehavior(lev)
		}
		if err != nil {
			return err
		}

		if len(lev.Scripts) > 0 {
			err = w.WriteLump("SCRIPTS", lev.Scripts)
			if err != nil {
				return err
			}
		}
	}

	if lev.Nodes != nil && lev.Nodes.HasGL {
		err = w.WriteLump(lev.Nodes.gl_marker_name, lev.Nodes.gl_marker_data)
		if err != nil {
			return err
		}

		err = w.WriteLump("GL_VERT", lev.Nodes.gl_vert)
		if err != nil {
			return err
		}

		err = w.WriteLump("GL_SEGS", lev.Nodes.gl_segs)
		if err != nil {
			return err
		}

		err = w.WriteLump("GL_SSECT", lev.Nodes.gl_ssect)
		if err != nil {
			return err
		}

		err = w.WriteLump("GL_NODES", lev.Nodes.gl_nodes)
		if err != nil {
			return err
		}

		err = w.WriteLump("GL_PVS", lev.Nodes.gl_pvs)
		if err != nil {
			return err
		}
	}

	return nil // OK
}

func (lev *Level) setFinalIndices() {
	for i, v := range lev.Verts {
		v.Index = i
	}

	for i, sd := range lev.Sides {
		sd.Index = i
	}

	for i, sec := range lev.Sectors {
		sec.Index = i
	}

	// NOTE: things and linedefs don't need a valid Index
}

func (w *Wad) writeThings(lev *Level) error {
	err := w.beginLump("THINGS")
	if err != nil {
		return err
	}

	for _, t := range lev.Things {
		switch lev.Format {
		case FORMAT_Doom:
			err = w.writeBunch(t.X, t.Y, t.Angle, t.Kind, t.Flags)

		case FORMAT_Hexen:
			err = w.writeBunch(t.Tid, t.X, t.Y, t.Height,
				t.Angle, t.Kind, t.Flags, t.Special, t.Args[:])
		}

		if err != nil {
			return err
		}
	}

	return w.endLump()
}

func (w *Wad) writeVertexes(lev *Level) error {
	err := w.beginLump("VERTEXES")
	if err != nil {
		return err
	}

	for _, v := range lev.Verts {
		err = w.writeBunch(v.X, v.Y)
		if err != nil {
			return err
		}
	}

	return w.endLump()
}

func (w *Wad) writeSectors(lev *Level) error {
	err := w.beginLump("SECTORS")
	if err != nil {
		return err
	}

	for _, s := range lev.Sectors {
		err = w.writeBunch(
			s.FloorH, s.CeilH,
			s.FloorTex, s.CeilTex, s.Light,
			s.Special, s.Tag)

		if err != nil {
			return err
		}
	}

	return w.endLump()
}

func (w *Wad) writeLineDefs(lev *Level) error {
	err := w.beginLump("LINEDEFS")
	if err != nil {
		return err
	}

	for _, ld := range lev.Lines {
		// sidedef pointers can be nil, which become 0xFFFF in the output
		front := _NO_SIDEDEF
		if ld.Front != nil {
			front = uint16(ld.Front.Index)
		}

		back := _NO_SIDEDEF
		if ld.Back != nil {
			back = uint16(ld.Back.Index)
		}

		switch lev.Format {
		case FORMAT_Doom:
			err = w.writeBunch(
				uint16(ld.Start.Index), uint16(ld.End.Index),
				ld.Flags, ld.Special, ld.Tag,
				front, back)

		case FORMAT_Hexen:
			err = w.writeBunch(
				uint16(ld.Start.Index), uint16(ld.End.Index),
				ld.Flags, uint8(ld.Special), ld.Args[:],
				front, back)
		}

		if err != nil {
			return err
		}
	}

	return w.endLump()
}

func (w *Wad) writeSideDefs(lev *Level) error {
	err := w.beginLump("SIDEDEFS")
	if err != nil {
		return err
	}

	for _, sd := range lev.Sides {
		err = w.writeBunch(
			sd.OffsetX, sd.OffsetY,
			sd.Upper, sd.Lower, sd.Mid,
			uint16(sd.Sector.Index))

		if err != nil {
			return err
		}
	}

	return w.endLump()
}

func (w *Wad) writeMinimalBehavior(lev *Level) error {
	// output a valid but empty ACS BEHAVIOR lump.
	err := w.beginLump("BEHAVIOR")
	if err != nil {
		return err
	}

	marker := [4]byte{'A', 'C', 'S', 0}

	err = w.writeBunch(
		marker[:],
		uint32(16), // dir offset
		uint32(0),  // code
		marker[:],  // string table
		uint32(0), uint32(0), uint32(1),
		uint32(0), uint32(12), uint32(0),
	)

	if err != nil {
		return err
	}

	return w.endLump()
}

func (w *Wad) beginLump(name string) error {
	name = strings.ToUpper(name)

	err := w.writePad4()
	if err != nil {
		return err
	}

	// read current position
	pos, err := w.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	lump := w.newLump()

	// length is determined later, use a dummy value for now
	lump.Start = uint32(pos)
	lump.Name = name

	return nil
}

func (w *Wad) endLump() error {
	if len(w.Directory) == 0 {
		panic("endLump called on empty directory")
	}

	// determine how many bytes were written

	lump := w.Directory[len(w.Directory)-1]

	pos, err := w.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	lump.Length = uint32(pos) - lump.Start

	// empty lumps don't need a real starting offset, use zero instead.
	// [ this is not compulsory, just a common convention ]
	if lump.Length == 0 {
		lump.Start = 0
	}

	return nil
}

/* UTILITIES */

// writeBunch is a variadic function which will handle 8/16/32-bit
// values and 8-byte strings and write them appropriately.  If any
// write produces an error, that error will be immediately returned.
func (w *Wad) writeBunch(a ...interface{}) error {
	for _, arg := range a {
		var err error

		switch val := arg.(type) {
		case uint8:
			err = w.write8(val)
		case uint16:
			err = w.write16(val)
		case uint32:
			err = w.write32(val)

		case int16:
			err = w.write16(uint16(val))
		case int32:
			err = w.write32(uint32(val))

		case ThingFlags:
			err = w.write16(uint16(val))
		case LineFlags:
			err = w.write16(uint16(val))

		case string:
			err = w.writeStr8(val)
		case []byte:
			err = writeExact(w.file, val)

		default:
			fmt.Printf("\nType not supported: %t\n", val)
			panic("writeBunch: unsupported type used")
		}
		if err != nil {
			return err
		}
	}
	return nil // OK
}

// write an 8-bit byte
func (w *Wad) write8(val uint8) error {
	var buf [1]byte
	buf[0] = val
	return writeExact(w.file, buf[:])
}

// write a 16-bit little endian value
func (w *Wad) write16(val uint16) error {
	var buf [2]byte
	buf[0] = byte(val)
	buf[1] = byte(val >> 8)
	return writeExact(w.file, buf[:])
}

// write a 32-bit little endian value
func (w *Wad) write32(val uint32) error {
	var buf [4]byte
	buf[0] = byte(val)
	buf[1] = byte(val >> 8)
	buf[2] = byte(val >> 16)
	buf[3] = byte(val >> 24)
	return writeExact(w.file, buf[:])
}

// write a short string padded to 8 bytes with zeros
func (w *Wad) writeStr8(str string) error {
	var buf [8]byte
	for i := 0; i < 8; i++ {
		if i >= len(str) {
			break
		}
		buf[i] = str[i]
	}
	return writeExact(w.file, buf[:])
}

// add zeros so each lump begins at a multiple of 4
func (w *Wad) writePad4() error {
	pos, err := w.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	ZEROS := [4]byte{0, 0, 0, 0}

	padding := (4 - (pos & 3)) & 3

	if padding > 0 {
		return writeExact(w.file, ZEROS[0:padding])
	}

	return nil
}

// this guarantees to write the whole buffer, or return an error.
// it *probably* is not needed, judging by the standard lib docs,
// but it exists just in case we ever need to handle short writes.
func writeExact(f *os.File, b []byte) error {
	_, err := f.Write(b)
	return err
}
