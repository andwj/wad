// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Package wad supports reading and writing DOOM WAD files.

by Andrew Apted, 2018.


Introduction

The focus of this library is reading and writing DOOM levels.
It is not designed to be a comprehensive WAD handling library,
the kind you would use to build a DOOM map editor or DOOM-like
game engine.

Hence there is no support at the moment for loading textures,
sounds, sprites or other kinds of data (beyond the basic ability
to read and write arbitrary lumps as raw bytes).

Also be aware that UDMF levels are NOT supported, they cannot be
read or written, however their presence is detected within a WAD
file.


License

Copyright (c) 2017 Andrew Apted

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package wad
