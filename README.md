
Package Wad
===========

*A Go package to support reading and writing DOOM WAD files.*

by Andrew Apted, 2018.


Introduction
------------

The focus of this library is reading and writing DOOM levels.
It is not designed to be a comprehensive WAD handling library,
the kind you would use to build a DOOM map editor or DOOM-like
game engine.

Hence there is no support at the moment for loading textures,
sounds, sprites or other kinds of data (beyond the basic ability
to read and write arbitrary lumps as raw bytes).

Also be aware that UDMF levels are NOT supported, they cannot be
read or written, however their presence is detected within a WAD
file.


Usage
-----

Please visit the [GoDoc page](https://www.godoc.org/gitlab.com/andwj/wad)
for documentation.

See the [examples](examples) directory for some examples.


Status
------

Version is 0.8.2

The functionality for reading and writing standard DOOM and
HEXEN levels is complete and seems to be working fine.

There are tests and they are all passing (here).  There are
also several examples of using this library.  Plus the code
documentation is fairly complete.


License
-------

The license is MIT-style, a permissive open source license.
See the [LICENSE.md](LICENSE.md) file for the complete text.

