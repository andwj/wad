// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package wad

import (
	"os"
)

// Wad represents a DOOM WAD file which was opened for reading or
// writing.  The internal directory will be read by Open(), and
// should not be modified afterwards.  For writing wads, it will
// be incrementally created as each lump is written, and the
// directory is only written when Finish() has been called.
type Wad struct {
	Directory []*Lump

	// the kind of wad (PWAD or IWAD) is marked in the wad header.
	// only the base game wads (like "DOOM2.WAD") are iwads, and
	// everything else (user made wads) are pwads.
	Iwad bool

	// for internal use
	file    *os.File
	isWrite bool
}

// Lump describes a single entry (file) within a WAD file.
// The contents of the lump are NOT stored here, but must be
// read in via ReadLump() or written via WriteLump().
//
// Names are limited to a maximum of 8 ASCII characters, and
// typically do not use spaces or punctuation symbols.  If you
// set a name longer than 8 characters, it will be silently
// truncated when writing.
type Lump struct {
	Start  uint32
	Length uint32
	Name   string

	// these are not part of the on-disk structure
	LevelMarker LevelFormat
	LevelPart   bool
	GLMarker    bool
}

// LevelFormat marks the detected format of a level, though
// most lumps will be set as NOT_A_LEVEL.
type LevelFormat byte

const (
	NOT_A_LEVEL LevelFormat = iota

	FORMAT_Doom
	FORMAT_Hexen
	FORMAT_UDMF
)

// Offsets for each lump in a level.  Levels are stored as a group
// of contiguous lumps, where the marker lump (also called "header"
// lump) is at offset zero.
const (
	LEVOF_Marker = iota // the ExMy or MAPxx marker
	LEVOF_Things
	LEVOF_LineDefs
	LEVOF_SideDefs
	LEVOF_Vertexes // same spelling as lump name
	LEVOF_Segs
	LEVOF_SSectors
	LEVOF_Nodes
	LEVOF_Sectors
	LEVOF_Reject
	LEVOF_Blockmap
	LEVOF_Behavior // Hexen only lump
	LEVOF_Scripts  // optional (usually absent), Hexen only
)

// Offsets for each lump in the GL-Nodes group, which usually directly
// follow the normal lumps of a level.
const (
	glofs_Marker = iota
	glofs_Vert
	glofs_Segs
	glofs_SSect
	glofs_Nodes
	glofs_PVS
)

// Level is the information for a single level in the WAD.
// It can be loaded from a wad via ReadLevel(), or created
// from scratch with NewLevel() and later written to a wad
// via WriteLevel().
//
// It is also possible to write a Level which has been loaded from
// an input wad into an output wad.  However, if any modifications
// have been made then it may be necessary to clear any existing
// nodes.  See the comment for "Nodes" for more details.
type Level struct {
	Name   string
	Format LevelFormat

	Things  []*Thing
	Verts   []*Vertex
	Sectors []*Sector
	Lines   []*LineDef
	Sides   []*SideDef

	// the marker lump of a level (MAP01 etc) can be used for certain
	// things, notably Fragglescript.  This preserves that data after
	// reading a level.
	HeaderData []byte

	// the NodeInfo is created when reading a level.  If the level
	// has not been modified, or only "minimally" modified, then this
	// information can be retained for a call to WriteLevel(),
	// otherwise this should be set to nil.
	Nodes *NodeInfo

	Behavior []byte
	Scripts  []byte
}

// Thing is a object in the level, such as a player, a monster, an item,
// or a decoration.  The 'Kind' field is what kind of object it is, and
// is a game-dependent number.
type Thing struct {
	X, Y  int16
	Angle int16

	Kind  uint16
	Flags ThingFlags

	// Hexen only fields
	Tid     uint16
	Height  uint16
	Special byte
	Args    [5]byte

	// this is only valid after reading a Level
	Index int
}

// Vertex is one of the end-points of a LineDef.  The coordinate system
// is similar to mathematical graphs, where X increases to the east and
// Y increases to the north.
type Vertex struct {
	X, Y int16

	// this is only valid after reading a Level
	Index int
}

// Sector is a region of space on the level, with a certain floor height
// and ceiling height.  The shape of sectors are implied by the LineDefs
// which refer to them.  Sectors which have no LineDefs referring to
// them can exist but are essentially useless.
type Sector struct {
	FloorH int16
	CeilH  int16

	FloorTex string
	CeilTex  string

	Light   uint16
	Special uint16
	Tag     int16

	// this is only valid after reading a Level
	Index int
}

// LineDef is just a line on the 2D map, though in the 3D gameworld it
// can represent a wall.  LineDefs have two sides, either of which may
// connect to a Sector via a SideDef.  Under normal circumstances
// every LineDef has a front side (sometimes called the "right" side),
// and may or may not have a back side (sometimes called the "left" side).
// The vertex pointers are always valid (never nil).
type LineDef struct {
	Start *Vertex
	End   *Vertex

	Flags   LineFlags
	Special uint16 // only 0-255 is supported in the Hexen format

	Front *SideDef // can be nil
	Back  *SideDef // can be nil

	// Doom only fields
	Tag int16

	// Hexen only fields
	Args [5]byte

	// this is only valid after reading a Level
	Index int
}

// _NO_SIDEDEF represents the lack of a sidedef on a linedef
// (in the on-disk structure).  Valid maps always have a
// front sidedef and may lack the back sidedef, though lines
// lacking both may occur during editing.
const _NO_SIDEDEF = uint16(0xffff)

// SideDef is used to describe the side of a LineDef, especially
// what textures to use on walls.  It is possible for a single
// SideDef to be used by multiple lines, this kind of "packing"
// can lead to problems and should be avoided.
// The Sector reference is always valid (never nil).
type SideDef struct {
	OffsetX int16
	OffsetY int16

	Upper string
	Lower string
	Mid   string

	Sector *Sector

	// this is only valid after reading a Level
	Index int
}

// NodeInfo collects all the information that is usually created
// by a nodes builder, including the BLOCKMAP and REJECT lumps.
type NodeInfo struct {
	nodes    []byte
	segs     []byte
	ssectors []byte
	reject   []byte
	blockmap []byte

	// this marks whether GL-Nodes were present
	HasGL bool

	gl_marker_name string
	gl_marker_data []byte

	gl_vert  []byte
	gl_segs  []byte
	gl_ssect []byte
	gl_nodes []byte
	gl_pvs   []byte
}

/* FLAG BITS */

// Flag bits for the LineDef struct.  Flags common to all DOOM-based
// games have the "LF_" prefix.  The "DLF_", "HLF_" and "ZLF_"
// prefixes are used for game-specific or engine-specific flags.
type LineFlags uint16

const (
	// common flags
	LF_Blocking LineFlags = (1 << iota)
	LF_BlockMonsters
	LF_TwoSided
	LF_UpperUnpegged
	LF_LowerUnpegged
	LF_Secret
	LF_SoundBlock
	LF_DontDraw
	LF_Mapped

	// Doom only flags
	DLF_PassThru // Boom engine
	DLF_3DMidTex // Eternity engine

	// Hexen only flags
	HLF_Repeatable     LineFlags = 0x0200
	HLF_ActivationMask LineFlags = 0x1c00

	// ZDoom only flags, usable in Hexen too
	ZLF_MonCanActivate LineFlags = 0x2000
	ZLF_BlockPlayers   LineFlags = 0x4000
	ZLF_BlockAll       LineFlags = 0x8000
)

// Hexen line activation types.
const (
	ACT_Cross   = 0
	ACT_Use     = 1
	ACT_Monster = 2
	ACT_Impact  = 3
	ACT_Push    = 4
	ACT_PCross  = 5

	ACT_SHIFT_BITS = 10
)

// Flag bits for a Thing definition.  Flags common to all DOOM-based
// games have the "TF_" prefix, whereas "DTF_" and "HTF_" are used
// for game-specific or engine-specific flags.
type ThingFlags uint16

const (
	// common flags
	TF_Easy ThingFlags = (1 << iota)
	TF_Medium
	TF_Hard
	TF_Ambush

	// Doom only flags
	DTF_Not_SP
	DTF_Not_DM   // Boom engine
	DTF_Not_COOP //

	DTF_Friend   // MBF engine
	DTF_Reserved //

	// Hexen only flags
	HTF_Dormant ThingFlags = 0x0010
	HTF_Fighter ThingFlags = 0x0020
	HTF_Cleric  ThingFlags = 0x0040
	HTF_Mage    ThingFlags = 0x0080

	HTF_SP   ThingFlags = 0x0100
	HTF_COOP ThingFlags = 0x0200
	HTF_DM   ThingFlags = 0x0400
)

/* CONSTRUCTORS */

// newWad creates a blank Wad structure.
func newWad(file *os.File, isWrite bool) *Wad {
	w := Wad{file: file, isWrite: isWrite}
	w.Directory = make([]*Lump, 0, 100)
	return &w
}

func (w *Wad) newLump() *Lump {
	lump := Lump{}
	w.Directory = append(w.Directory, &lump)
	return &lump
}

func NewLevel(name string, format LevelFormat) *Level {
	lev := Level{Name: name, Format: format}

	lev.Things = make([]*Thing, 0, 100)
	lev.Verts = make([]*Vertex, 0, 100)
	lev.Sectors = make([]*Sector, 0, 100)
	lev.Lines = make([]*LineDef, 0, 100)
	lev.Sides = make([]*SideDef, 0, 100)

	return &lev
}

func (lev *Level) NewThing() *Thing {
	t := Thing{}
	lev.Things = append(lev.Things, &t)
	return &t
}

func (lev *Level) NewVertex() *Vertex {
	v := Vertex{}
	lev.Verts = append(lev.Verts, &v)
	return &v
}

func (lev *Level) NewSector() *Sector {
	s := Sector{}
	lev.Sectors = append(lev.Sectors, &s)
	return &s
}

func (lev *Level) NewLineDef() *LineDef {
	ld := LineDef{}
	lev.Lines = append(lev.Lines, &ld)
	return &ld
}

func (lev *Level) NewSideDef() *SideDef {
	sd := SideDef{Upper: "-", Mid: "-", Lower: "-"}
	lev.Sides = append(lev.Sides, &sd)
	return &sd
}

/* COMPARISONS */

func (t *Thing) Equal(other *Thing) bool {
	return true &&
		(t.X == other.X) &&
		(t.Y == other.Y) &&
		(t.Angle == other.Angle) &&
		(t.Kind == other.Kind) &&
		(t.Flags == other.Flags) &&
		(t.Tid == other.Tid) &&
		(t.Height == other.Height) &&
		(t.Special == other.Special) &&
		(t.Args == other.Args)
}

func (v *Vertex) Equal(other *Vertex) bool {
	return (v.X == other.X) && (v.Y == other.Y)
}

func (s *Sector) Equal(other *Sector) bool {
	return true &&
		(s.FloorH == other.FloorH) &&
		(s.CeilH == other.CeilH) &&
		(s.FloorTex == other.FloorTex) &&
		(s.CeilTex == other.CeilTex) &&
		(s.Light == other.Light) &&
		(s.Special == other.Special) &&
		(s.Tag == other.Tag)
}

func (sd *SideDef) Equal(other *SideDef) bool {
	return true &&
		(sd.OffsetX == other.OffsetX) &&
		(sd.OffsetY == other.OffsetY) &&
		(sd.Upper == other.Upper) &&
		(sd.Lower == other.Lower) &&
		(sd.Mid == other.Mid) &&
		sd.Sector.Equal(other.Sector)
}

func (ld *LineDef) Equal(other *LineDef) bool {
	if ld.Front != other.Front {
		if ld.Front == nil || other.Front == nil {
			return false
		} else if !ld.Front.Equal(other.Front) {
			return false
		}
	}

	if ld.Back != other.Back {
		if ld.Back == nil || other.Back == nil {
			return false
		} else if !ld.Back.Equal(other.Back) {
			return false
		}
	}

	return true &&
		ld.Start.Equal(other.Start) &&
		ld.End.Equal(other.End) &&
		(ld.Flags == other.Flags) &&
		(ld.Special == other.Special) &&
		(ld.Tag == other.Tag) &&
		(ld.Args == other.Args)
}
