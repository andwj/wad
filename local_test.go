// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Tests for the wad package.

There are four tests here, and they are all closely tied
to the wad file in the data/ directory, which is a DOOM
level obtained from the FreeDoom project (which has a
permissive BSD-style license).

1. TestDirectory checks that the wad file could be opened,
the internal directory could be read, and that it contains
the correct information.

2. TestReadLump checks the ReadLump() method, reading a
handful of lumps from the wad and checking that the CRC32
checksums match the ones determined independently.

3. TestReadLevel checks that the ReadLevel() method can
decode the MAP01 level in the wad, checking that various
parts of the level data are correct.

4. TestCopyLevel checks the WriteLevel() method is working,
by copying the MAP01 level into a newly created wad, and
then loading the second wad to verify that the level data
is the same.

*/

package wad

import (
	"hash/crc32"
	"os"
	"path/filepath"
	"testing"
)

func TestDirectory(t *testing.T) {
	w, err := Open("data/freedoom_map.wad")
	if err != nil {
		t.Errorf("Open() returned an error: %s", err.Error())
		return
	}

	defer w.Close()

	if len(w.Directory) < 11 {
		t.Errorf("Wad directory is short (%d < %d lumps)", len(w.Directory), 11)
		return
	}

	checkInfo := func(i int, name string, size uint32) {
		if w.Directory[i].Name != name {
			t.Errorf("Lump name [%d] should be '%s', got '%s'", i, name, w.Directory[i].Name)
		}

		if w.Directory[i].Length != size {
			t.Errorf("Lump size [%d] should be %d, got %d", i, size, w.Directory[i].Length)
		}
	}

	checkInfo(0, "MAP01", 0)
	checkInfo(1, "THINGS", 1030)
	checkInfo(2, "LINEDEFS", 9142)
	checkInfo(3, "SIDEDEFS", 28740)
	checkInfo(4, "VERTEXES", 2524)
	checkInfo(5, "SEGS", 13236)
	checkInfo(6, "SSECTORS", 1124)
	checkInfo(7, "NODES", 7840)
	checkInfo(8, "SECTORS", 3380)
	checkInfo(9, "REJECT", 2113)
	checkInfo(10, "BLOCKMAP", 3122)

	if w.Directory[0].LevelMarker != FORMAT_Doom {
		t.Errorf("First entry not detected as a DOOM map")
	}
}

func TestReadLump(t *testing.T) {
	w, err := Open("data/freedoom_map.wad")
	if err != nil {
		t.Errorf("Open() returned an error: %s", err.Error())
		return
	}

	defer w.Close()

	if len(w.Directory) < 11 {
		t.Errorf("Wad directory is short (%d < %d lumps)", len(w.Directory), 11)
		return
	}

	checkCRC := func(match uint32, name string) {
		lump := w.FindLump(name)
		if lump == nil {
			t.Errorf("Missing '%s' lump in wad", name)
			return
		}

		data, err := w.ReadLump(lump)
		if err != nil {
			t.Errorf("ReadLump for '%s' failed: %s", name, err.Error())
			return
		}

		crc := crc32.ChecksumIEEE(data)

		if crc != match {
			t.Errorf("Wrong checksum for '%s', should be %08x, got %08x\n",
				name, match, crc)
			return
		}
	}

	// these CRC values created by extracting the lumps with "wadext" of the
	// XWadTools package, and running the "crc32" program from Archive::Zip
	// module for perl.
	checkCRC(0x00000000, "MAP01")
	checkCRC(0xf6501bb4, "REJECT")
	checkCRC(0x837d60a6, "SIDEDEFS")
	checkCRC(0x41cb0b0b, "NODES")
}

func TestReadLevel(t *testing.T) {
	w, err := Open("data/freedoom_map.wad")
	if err != nil {
		t.Errorf("Open() returned an error: %s", err.Error())
		return
	}

	defer w.Close()

	if len(w.Directory) < 11 {
		t.Errorf("Wad directory is short (%d < %d lumps)", len(w.Directory), 11)
		return
	}

	lev, err := w.ReadLevel(0)
	if err != nil {
		t.Errorf("ReadLevel() returned an error: %s", err.Error())
		return
	}

	checkLen := func(name string, got, need int) bool {
		if got == need {
			return true
		}
		t.Errorf("Wrong number of %s: should be %d, got %d", name, need, got)
		return false
	}

	lensOk := true &&
		checkLen("Things", len(lev.Things), 103) &&
		checkLen("LineDefs", len(lev.Lines), 653) &&
		checkLen("SideDefs", len(lev.Sides), 958) &&
		checkLen("Vertexes", len(lev.Verts), 631) &&
		checkLen("Sectors", len(lev.Sectors), 130)

	if !lensOk {
		return
	}
}

func TestCopyLevel(t *testing.T) {
	// determine the place where we can save a WAD
	tmpDir := os.TempDir()

	outName := filepath.Join(tmpDir, "testing_map.wad")

	in, err := Open("data/freedoom_map.wad")
	if err != nil {
		t.Errorf("Open() returned an error: %s", err.Error())
		return
	}

	defer in.Close()

	if len(in.Directory) < 10 || in.Directory[0].LevelMarker != FORMAT_Doom {
		t.Errorf("Missing level in input wad")
		return
	}

	lev1, err := in.ReadLevel(0)
	if err != nil {
		t.Errorf("ReadLevel() returned an error: %s", err.Error())
		return
	}

	out, err := Create(outName)
	if err != nil {
		t.Errorf("Create() returned an error: %s", err.Error())
		return
	}

	// this defer will delete the created file
	defer func() {
		os.Remove(outName)
	}()

	if err := out.WriteLevel(lev1); err != nil {
		t.Errorf("WriteLevel() returned an error: %s", err.Error())
		out.Close()
		return
	}

	if err := out.Finish(); err != nil {
		t.Errorf("WriteLevel() returned an error: %s", err.Error())
		out.Close()
		return
	}

	out.Close()

	// open the newly created file for reading
	out, err = Open(outName)
	if err != nil {
		t.Errorf("Open() the new wad failed: %s", err.Error())
		return
	}

	defer out.Close()

	if len(out.Directory) < 10 || out.Directory[0].LevelMarker != FORMAT_Doom {
		t.Errorf("Missing level in output wad")
		return
	}

	lev2, err := out.ReadLevel(0)
	if err != nil {
		t.Errorf("ReadLevel() on new wad failed: %s", err.Error())
		return
	}

	// compare the two levels...

	if lev2.Name != lev1.Name {
		t.Errorf("Copy has wrong level name, should be '%s', got '%s'",
			lev1.Name, lev2.Name)
	}

	checkCount := func(name string, lev1, lev2 int) {
		if lev1 != lev2 {
			t.Errorf("Wrong count in '%s' lump, should be %d, got %d",
				name, lev1, lev2)
		}
	}

	checkCount("THINGS", len(lev1.Things), len(lev2.Things))
	checkCount("LINEDEFS", len(lev1.Lines), len(lev2.Lines))
	checkCount("SIDEDEFS", len(lev1.Sides), len(lev2.Sides))
	checkCount("VERTEXES", len(lev1.Verts), len(lev2.Verts))
	checkCount("SECTORS", len(lev1.Sectors), len(lev2.Sectors))

	for i, t1 := range lev1.Things {
		t2 := lev2.Things[i]
		if !t1.Equal(t2) {
			t.Errorf("Things at [%d] are not equal", i)
			break
		}
	}

	for i, v1 := range lev1.Verts {
		v2 := lev2.Verts[i]
		if !v1.Equal(v2) {
			t.Errorf("Vertices at [%d] are not equal", i)
			break
		}
	}

	for i, s1 := range lev1.Sectors {
		s2 := lev2.Sectors[i]
		if !s1.Equal(s2) {
			t.Errorf("Sectors at [%d] are not equal", i)
			break
		}
	}

	for i, sd1 := range lev1.Sides {
		sd2 := lev2.Sides[i]
		if !sd1.Equal(sd2) {
			t.Errorf("SideDefs at [%d] are not equal", i)
			break
		}
	}

	for i, ld1 := range lev1.Lines {
		ld2 := lev2.Lines[i]
		if !ld1.Equal(ld2) {
			t.Errorf("LineDefs at [%d] are not equal", i)
			break
		}
	}
}
