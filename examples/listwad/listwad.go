// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

listwad is a simple program that lists the contents of a WAD.

*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/andwj/wad"
)

func main() {
	if len(os.Args) < 2 ||
		os.Args[1] == "-h" ||
		os.Args[1] == "-help" ||
		os.Args[1] == "--help" {
		fmt.Fprintf(os.Stderr, "USAGE: listwad <file>\n")
		os.Exit(1)
	}

	filename := os.Args[1]

	w, err := wad.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open wad file: %s\n", filename)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	// list the lumps
	for i, lump := range w.Directory {
		fmt.Printf("%4d:  %-8s %8d bytes   at 0x%08x\n",
			i, lump.Name, lump.Length, lump.Start)
	}

	w.Close()
}
