// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

transmogrify extracts the levels from the input wad and
converts them to the other format in the output wad (i.e.
DOOM format is converted to HEXEN format and vice versa).

NOTE: no "real" conversion is done, such as thing types,
texture names, or linedef specials.  Also the conversion
will be lossy, e.g. Hexen format things will lose several
fields when converted to DOOM format.

*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/andwj/wad"
)

func main() {
	if len(os.Args) < 3 ||
		os.Args[1] == "-h" ||
		os.Args[1] == "-help" ||
		os.Args[1] == "--help" {
		fmt.Fprintf(os.Stderr, "USAGE: transmogrify <input> <output>\n")
		os.Exit(1)
	}

	in, err := wad.Open(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open input file: %s\n", os.Args[1])
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	out, err := wad.Create(os.Args[2])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create output file: %s\n", os.Args[2])
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		in.Close()
		os.Exit(1)
	}

	count := 0

	for i, lump := range in.Directory {
		if lump.LevelMarker != wad.NOT_A_LEVEL {
			processLevel(in, out, i, lump)
			count += 1
		}
	}

	in.Close()

	out.Finish()
	out.Close()

	if count == 0 {
		fmt.Println("No levels found in wad.")
	}
}

func processLevel(in *wad.Wad, out *wad.Wad, lumpIndex int, lump *wad.Lump) {
	lev, err := in.ReadLevel(lumpIndex)

	if err != nil {
		fmt.Printf("read %s: --> %s\n", lump.Name, err.Error())
		return
	}

	oldFormat := lev.Format

	if oldFormat == wad.FORMAT_Doom {
		lev.Format = wad.FORMAT_Hexen
	} else {
		lev.Format = wad.FORMAT_Doom
	}

	// do minimal conversion of Tag <--> Args in LineDefs
	for _, ld := range lev.Lines {
		tag := byte(ld.Tag)
		arg := int16(ld.Args[0])

		ld.Tag, ld.Args[0] = arg, tag
	}

	err = out.WriteLevel(lev)
	if err != nil {
		fmt.Printf("write %s: --> %s\n", lump.Name, err.Error())
		return
	}

	fmt.Printf("converted %s\n", lump.Name)
}
