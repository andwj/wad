// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

levelstats shows some information about each level in the WAD.

*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/andwj/wad"
)

func main() {
	if len(os.Args) < 2 ||
		os.Args[1] == "-h" ||
		os.Args[1] == "-help" ||
		os.Args[1] == "--help" {
		fmt.Fprintf(os.Stderr, "USAGE: levelstats <file>\n")
		os.Exit(1)
	}

	filename := os.Args[1]

	w, err := wad.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open wad file: %s\n", filename)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	count := 0

	for i, lump := range w.Directory {
		if lump.LevelMarker != wad.NOT_A_LEVEL {
			showLevel(w, i, lump)
			count += 1
		}
	}

	w.Close()

	if count == 0 {
		fmt.Println("No levels found in wad.")
	}
}

func showLevel(w *wad.Wad, lumpIndex int, lump *wad.Lump) {
	lev, err := w.ReadLevel(lumpIndex)

	if err != nil {
		fmt.Printf("%s: --> %s\n", lump.Name, err.Error())
		fmt.Println()
		return
	}

	fmt.Printf("%s:\n", lump.Name)

	switch lev.Format {
	case wad.FORMAT_Doom:
		fmt.Println("   format is DOOM")
	case wad.FORMAT_Hexen:
		fmt.Println("   format is HEXEN")
	default:
		fmt.Println("   bad format!!")
	}

	fmt.Printf("   things: %d\n", len(lev.Things))
	fmt.Printf("   vertices: %d\n", len(lev.Verts))
	fmt.Printf("   sectors: %d\n", len(lev.Sectors))
	fmt.Printf("   linedefs: %d\n", len(lev.Lines))

	fmt.Println()
}
