// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

extractor is a program that saves the contents of a single
lump in a WAD to a file.  The result is the raw binary data,
no conversion at all is done.  The file is created in the
current directory.

*/
package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/andwj/wad"
)

func main() {
	if len(os.Args) < 3 ||
		os.Args[1] == "-h" ||
		os.Args[1] == "-help" ||
		os.Args[1] == "--help" {
		fmt.Fprintf(os.Stderr, "USAGE: extractor <file> <lump>\n")
		os.Exit(1)
	}

	filename := os.Args[1]

	w, err := wad.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open wad file: %s\n", filename)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	lumpName := strings.ToUpper(os.Args[2])

	lump := w.FindLump(lumpName)

	if lump == nil {
		fmt.Fprintf(os.Stderr, "No such lump '%s' in the wad file\n", lumpName)
		w.Close()
		os.Exit(1)
	}

	data, err := w.ReadLump(lump)

	// we can close the wad now
	w.Close()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading input file: %s\n", filename)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	// create output filename from the lump name
	outName := lumpName + ".raw"

	f, err := os.Create(outName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create output file: %s\n", outName)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		os.Exit(1)
	}

	_, err = f.Write(data)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error writing to output file: %s\n", outName)
		fmt.Fprintf(os.Stderr, "--> %s\n", err.Error())
		f.Close()
		os.Exit(1)
	}

	fmt.Printf("Saved %d bytes to file: %s\n", len(data), outName)

	f.Close()
}
